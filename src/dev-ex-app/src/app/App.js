import React from 'react'

import { makeStyles } from '@material-ui/core/styles'

import Landing from '../features/landing/Landing'

const useStyles = makeStyles((theme) => ({
  root: { margin: theme.spacing(2), display: 'flex' }
}))

export default () => {
  const classes = useStyles()

  return (
    <div className={classes.root}>
      <Landing />
    </div>
  )
}
