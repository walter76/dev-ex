import React from 'react'

import { Typography } from '@material-ui/core'

export default () => (
  <Typography variant="h5" noWrap>
    Landing Page
  </Typography>
)
