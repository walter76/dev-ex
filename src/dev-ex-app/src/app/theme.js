import { red } from '@material-ui/core/colors'
import { createMuiTheme } from '@material-ui/core/styles'

// https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=006064&secondary.color=795548
export default createMuiTheme({
  palette: {
    primary: {
      light: '#428e92',
      main: '#006064',
      dark: '#00363a',
      contrastText: '#fff'
    },
    secondary: {
      light: '#a98274',
      main: '#795548',
      dark: '#4b2c20',
      contrastText: '#fff'
    },
    error: {
      main: red.A400
    },
    background: {
      default: '#fff'
    }
  }
})
